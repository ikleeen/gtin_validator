# GTIN validator 

### Build
-------------
Compile module:
```sh
    $ make all
```
Run tests:
```sh
    $ make test
```
### Run
-------------
To run erlang 
```sh
    $ make run
```
Run module function
```sh
    $ validator:validate({gtin,<<"12345678">>}).
```