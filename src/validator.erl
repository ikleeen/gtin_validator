
-module(validator).

-export([
	validate/1
	]).

-define(ZERO_IOLIST, 48).
-define(MAX_UINT, 10).

-define(LENGTH_GTIN8, 8).
-define(LENGTH_GTIN12, 12).
-define(LENGTH_GTIN13, 13).
-define(LENGTH_GTIN14, 14). 

convert_binary(<<>>, List) -> List;
convert_binary(<<DIGIT/unsigned, Tail/binary>>, List) when (DIGIT < ?MAX_UINT) ->
	convert_binary(Tail, List ++ [DIGIT]);
convert_binary(_BS, _List) ->
	{error, "Invalid digit in given GTIN"}.

convert_iolist(<<>>, List) -> List;
convert_iolist(<<DIGIT/unsigned, Tail/binary>>, List) when (( DIGIT - ?ZERO_IOLIST) >= 0) and
											 (( DIGIT - ?ZERO_IOLIST) < ?MAX_UINT)  ->
	convert_iolist(Tail, List ++ [DIGIT - ?ZERO_IOLIST]);
convert_iolist(_BS, _List) ->
	{error, "Invalid digit in given GTIN"}.


convert(<<DIGIT1/unsigned, DIGIT2/unsigned, DIGIT3/unsigned, DIGIT4/unsigned, Tail/binary>>, ?LENGTH_GTIN12, AddinPad) when (DIGIT1 =/= 0) and (DIGIT1 =/= 48) or
																											(DIGIT2 =/= 0) and (DIGIT2 =/= 48) or
																											(DIGIT3 =/= 0) and (DIGIT3 =/= 48) or
																											(DIGIT4 =/= 0) and (DIGIT4 =/= 48) ->

	if 
		(DIGIT1 < ?MAX_UINT) and 
		(DIGIT2 < ?MAX_UINT) and
		(DIGIT3 < ?MAX_UINT) and
		(DIGIT4 < ?MAX_UINT) -> convert_binary(Tail, AddinPad ++ [DIGIT1, DIGIT2, DIGIT3, DIGIT4]);		
		(( DIGIT1 - ?ZERO_IOLIST) < ?MAX_UINT)  and 
		(( DIGIT2 - ?ZERO_IOLIST) < ?MAX_UINT)  and
		(( DIGIT3 - ?ZERO_IOLIST) < ?MAX_UINT)  and
	    (( DIGIT4 - ?ZERO_IOLIST) < ?MAX_UINT)  -> convert_iolist(Tail, AddinPad ++ [DIGIT1 - ?ZERO_IOLIST, 
	    																				DIGIT2 - ?ZERO_IOLIST, 
	    																				DIGIT3 - ?ZERO_IOLIST, 
	    																				DIGIT4 - ?ZERO_IOLIST]);
	    true -> {error, "Invalid digit in given GTIN"}
	end;    														

convert(<<DIGIT/unsigned, Tail/binary>>, _S, AddinPad) when (DIGIT < ?MAX_UINT) and
												(DIGIT > 0)  ->
	convert_binary(Tail, AddinPad ++ [DIGIT]);
convert(<<DIGIT/unsigned, Tail/binary>>, _S, AddinPad) when (( DIGIT - ?ZERO_IOLIST) > 0) and
											 (( DIGIT - ?ZERO_IOLIST) < ?MAX_UINT)  ->
	convert_iolist(Tail, AddinPad ++ [DIGIT-?ZERO_IOLIST]);
convert(_BS, _S, _AP) ->
	{error, "Invalid firsts digit in given GTIN"}.
	

calculate(N1, S, [], Sum) ->
			if 
				((N1*3 + Sum + S) rem ?MAX_UINT) =:= 0	-> ok;
				true -> error
			end;
calculate(N1, N2, [N3,N4|Tail], S) ->
	calculate(N3, N4, Tail, N1*3 + N2 + S).
	


check_control_sum([N1,N2|Tail])-> 
	calculate(N1, N2, Tail, 0).


check_prefix([0, 0, 0, 0, 0, 0, N7, N8, N9|_Tail]) ->
	Prefix = N7 * 100 + N8 * 10 + N9,
	if 
		(Prefix >= 0) and (Prefix =< 99) -> error;
		(Prefix >= 100) and (Prefix =< 139) -> ok;
		(Prefix >= 140) and (Prefix =<199) -> error;
		(Prefix >= 200) and (Prefix =< 299) -> error;
		(Prefix >= 300) and (Prefix =< 959) -> ok;
		(Prefix >= 960) and (Prefix =< 969) -> ok;
		(Prefix >= 970) and (Prefix =< 999) -> error;
		true -> error
	end;
check_prefix([_N1, N2, N3, N4|_Tail]) ->
	Prefix = N2 * 100 + N3 * 10 + N4,
	if 
		(Prefix >= 0) and (Prefix =< 19) -> ok;
		(Prefix >= 20) and (Prefix =< 29) -> error;
		(Prefix >= 30) and (Prefix =<39) -> ok;
		(Prefix >= 40) and (Prefix =< 49) -> error;
		(Prefix >= 50) and (Prefix =< 59) -> error;
		(Prefix >= 60) and (Prefix =< 139) -> ok;
		(Prefix >= 140) and (Prefix =< 199) -> error;
		(Prefix >= 200) and (Prefix =< 299) -> ok;
		(Prefix >= 300) and (Prefix =< 949) -> ok; %%%
		(Prefix >= 950) and (Prefix =< 951) -> ok;
		(Prefix >= 952) and (Prefix =< 954) -> ok;	
		(Prefix == 955)  -> ok;	
		(Prefix >= 956) and (Prefix =< 957) -> error;	
		(Prefix == 958)  -> ok;	
		(Prefix == 959)  -> error;	
		(Prefix >= 970) and (Prefix =< 976) -> error;	
		(Prefix == 977)  -> ok;	
		(Prefix >= 978) and (Prefix =< 979) -> error;	
		(Prefix >= 980) and (Prefix =< 999) -> error;	
		true -> error
	end.	

%% OOPS Not implemented yet
check_company_prefix(_List) ->
	ok.

validate({gtin, GTIN}) when is_binary(GTIN) ->
	Size = bit_size(GTIN) div 8,
	List = case Size of
				?LENGTH_GTIN8  -> convert(GTIN, Size,  [0,0,0,0,0,0]);
				?LENGTH_GTIN12 -> convert(GTIN, Size, [0,0]);
				?LENGTH_GTIN13 -> convert(GTIN, Size, [0]);
				?LENGTH_GTIN14 -> convert(GTIN, Size, []);
				_Other -> {error, "Invalid format of GTIN - invalid length."}
			end,
								
	PassLambda = fun (_FUN, {error, Reason}, _ERR_RET) -> {error, Reason};
					(FUN, PARAM, ERR_RET) when is_list(PARAM) -> 
						case FUN(PARAM) of 
							ok -> PARAM;
							error ->  Msg = "Bad validate " ++ ERR_RET,
									{error, Msg}
							
						end
				end, 		
	case  PassLambda(fun check_control_sum/1, 
					PassLambda(fun check_company_prefix/1, 
							PassLambda(fun check_prefix/1, List, "prefix"), 
					"company prefix"),
			"control sum") of
		{error, Reason} -> {error, Reason};
		_OK -> ok
	end;
validate(_GTIN) ->  {error, "Can validate only binary or iolist formats"}.	

%%%%%%%%%%%%%%%%% TESTS

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

gtin0_test() -> {error,_R} = validate({gtin,<<1,2,3,5,7,0,1>>}).
gtin1_test() -> {error,_R} = validate({gtin,<<1,2,3,5,7,0,1,0,0,0,0,0,0,0,0,0>>}).
gtin2_test() -> {error,_R} = validate({gtin,atom}).
gtin3_test() -> {error,_R} = validate({gtin,12}).
gtin4_test() -> {error,_R} = validate({gtin,"5,0,1,8,4,3,8,5"}).

gtin8_convert1_test() ->
	[0,0,0,0,0,0,1,2,3,4,5,7,0,1] = convert(<<1,2,3,4,5,7,0,1>>, bit_size(<<1,2,3,4,5,7,0,1>>) div 8,  [0,0,0,0,0,0]).
gtin8_convert2_test() ->
	[0,0,0,0,0,0,5,0,1,8,4,3,8,5] = convert(<<"50184385">>, bit_size(<<"50184385">>) div 8,  [0,0,0,0,0,0]).	
gtin8_convert3_test() ->
	{error, _R} = convert(<<5,0,"1",8,4,3,8,5>>, bit_size(<<5,0,"1",8,4,3,8,5>>) div 8,  [0,0,0,0,0,0]).	
gtin8_convert4_test() ->
	{error, _R} = convert(<<"5018a385">>, bit_size(<<"5018a385">>) div 8,  [0,0,0,0,0,0]).
gtin8_convert5_test() ->
	{error, _R}  = convert(<<"s0184385">>, bit_size(<<"s0184385">>) div 8,  [0,0,0,0,0,0]).	
gtin8_convert6_test() ->
	{error, _R} = convert(<<0,0,1,8,4,3,8,9>>, bit_size(<<0,0,1,8,4,3,8,9>>) div 8,  [0,0,0,0,0,0]).	

gtin12_convert1_test() ->
	[0,0,0,1,2,3,4,5,6,7,8,9,0,5] = convert(<<0,1,2,3,4,5,6,7,8,9,0,5>>, bit_size(<<0,1,2,3,4,5,6,7,8,9,0,5>>) div 8,  [0,0]).
gtin12_convert2_test() ->
	{error, _R} = convert(<<0,0,0,0,4,5,6,7,8,9,0,5>>, bit_size(<<0,0,0,0,4,5,6,7,8,9,0,5>>) div 8,  [0,0]).
gtin12_convert3_test() ->
	{error, _R} = convert(<<"000045678905">>, bit_size(<<"000045678905">>) div 8,  [0,0]).	
gtin12_convert4_test() ->
	[0,0,0,1,2,3,4,5,6,7,8,9,0,5] = convert(<<"012345678905">>, bit_size(<<"012345678905">>) div 8,  [0,0]).

gtin13_convert1_test() ->
	{error, _R} = convert(<<0,1,1,2,3,4,5,6,7,8,9,0,5>>, bit_size(<<0,1,1,2,3,4,5,6,7,8,9,0,5>>) div 8,  [0]).
gtin13_convert2_test() ->
	[0,1,1,1,2,3,4,5,6,7,8,9,0,5] = convert(<<1,1,1,2,3,4,5,6,7,8,9,0,5>>, bit_size(<<1,1,1,2,3,4,5,6,7,8,9,0,5>>) div 8,  [0]).
gtin13_convert3_test() ->
	[0,1,1,1,2,3,4,5,6,7,8,9,0,5] = convert(<<"1112345678905">>, bit_size(<<"1112345678905">>) div 8,  [0]).	
gtin13_convert4_test() ->
	{error, _R} = convert(<<"0112345678905">>, bit_size(<<"0112345678905">>) div 8,  [0]).
gtin13_convert5_test() ->
	{error, _R} = convert(<<1,1,1,2,3,4,5,16,7,8,9,0,5>>, bit_size(<<1,1,1,2,3,4,5,16,7,8,9,0,5>>) div 8,  [0]).		

gtin14_convert1_test() ->
	{error, _R} = convert(<<0,1,1,1,2,3,4,5,6,7,8,9,0,5>>, bit_size(<<0,1,1,1,2,3,4,5,6,7,8,9,0,5>>) div 8,  []).
gtin14_convert2_test() ->
	[1,1,1,1,2,3,4,5,6,7,8,9,0,5] = convert(<<1,1,1,1,2,3,4,5,6,7,8,9,0,5>>, bit_size(<<1,1,1,1,2,3,4,5,6,7,8,9,0,5>>) div 8,  []).
gtin14_convert3_test() ->
	[1,1,1,1,2,3,4,5,6,7,8,9,0,5] = convert(<<"11112345678905">>, bit_size(<<"11112345678905">>) div 8,  []).	
gtin14_convert4_test() ->
	{error, _R} = convert(<<"01112345678905">>, bit_size(<<"01112345678905">>) div 8,  [0]).
gtin14_convert5_test() ->
	{error, _R} = convert(<<1,1,1,1,2,3,4,5,16,7,8,9,0,5>>, bit_size(<<1,1,1,1,2,3,4,5,16,7,8,9,0,5>>) div 8,  []).

gtin8_check_control_sum1_test() -> ok = check_control_sum([0,0,0,0,0,0,1,4,0,1,1,9,6,2]).
gtin8_check_control_sum2_test() -> error = check_control_sum([0,0,0,0,0,0,1,4,0,1,1,9,6,3]).
gtin8_check_control_sum3_test() -> ok = check_control_sum([0,0,0,0,0,0,9,3,1,2,3,4,5,7]).
gtin8_check_control_sum4_test() -> error = check_control_sum([0,0,0,0,0,0,9,3,1,2,3,4,5,0]).

gtin12_check_control_sum1_test()  -> ok = check_control_sum([0,0,8,9,2,6,8,5,0,0,1,0,0,3]).
gtin12_check_control_sum2_test()  -> ok = check_control_sum([0,0,0,3,6,0,0,0,2,9,1,4,5,2]).
gtin12_check_control_sum3_test()  -> error = check_control_sum([0,0,0,3,6,0,0,0,2,9,1,4,5,0]).
gtin12_check_control_sum4_test()  -> error = check_control_sum([0,0,8,9,2,6,8,5,0,0,1,0,0,4]).

gtin13_check_control_sum1_test()  -> ok = check_control_sum([0,0,0,1,2,3,4,5,6,7,8,9,0,5]).
gtin13_check_control_sum2_test()  -> error = check_control_sum([0,0,0,1,2,3,4,5,6,7,8,9,0,4]).
gtin13_check_control_sum3_test()  -> ok = check_control_sum([0,5,9,0,1,2,3,4,1,2,3,4,5,7]).
gtin13_check_control_sum4_test()  -> error = check_control_sum([0,5,9,0,1,2,3,4,1,2,3,4,5,6]).

gtin14_check_control_sum1_test()  -> ok = check_control_sum([1,0,3,8,4,4,7,8,8,6,1,8,0,4]).
gtin14_check_control_sum2_test()  -> error = check_control_sum([1,1,3,8,4,4,7,8,8,6,1,8,0,4]).
gtin14_check_control_sum3_test()  -> ok = check_control_sum([1,0,0,1,2,3,4,5,6,7,8,9,0,2]).
gtin14_check_control_sum4_test()  -> error = check_control_sum([1,0,0,2,2,3,4,5,6,7,8,9,0,2]).

gtin5_test() -> ok = validate({gtin,<<5,7,0,1,2,3,4,6>>}).
gtin6_test() -> ok = validate({gtin,<<"50184385">>}).
gtin7_test() -> {error,_R} = validate({gtin,<<1,4,0,1,1,9,6,2>>}).
gtin8_test() -> ok = validate({gtin,<<8,1,1,5,7,1,0,1,3,5,7,9>>}).
gtin9_test() -> ok = validate({gtin,<<"811571013579">>}).
gtin10_test() -> {error,_R} = validate({gtin,<<8,1,1,5,7,1,0,1,3,5,7,7>>}).
gtin11_test() -> ok = validate({gtin,<<"98765432109213">>}).
gtin12_test() -> ok = validate({gtin,<<9,8,7,6,5,4,3,2,1,0,9,2,1,3>>}).
gtin13_test() -> ok = validate({gtin,<<5,9,0,1,2,3,4,1,2,3,4,5,7>>}).
gtin14_test() -> {error,_R} = validate({gtin,<<0,0,0,1,2,3,4,5,6,7,8,9,0,5>>}).

-endif.